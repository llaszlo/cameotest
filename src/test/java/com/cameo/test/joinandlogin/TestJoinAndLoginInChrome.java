package com.cameo.test.joinandlogin;

import com.cameo.configuration.BrowserSettings;
import com.cameo.factory.WebDriverFactory;
import com.cameo.test.base.JoinAndLogin;
import org.openqa.selenium.WebDriver;

public class TestJoinAndLoginInChrome extends JoinAndLogin {

    protected WebDriver getDriver() {
        System.setProperty("webdriver."+BrowserSettings.WEBDRIVER_CHROME_DRIVER+".driver", BrowserSettings.CHROME_DRIVER_LOCATION);
        browserType = BrowserSettings.CHROME_BROWSER_TYPE;
        return WebDriverFactory.getWebDriver(WebDriverFactory.WebDrivers.CHROME_DRIVER);
    }

}