package com.cameo.test.joinandlogin;

import com.cameo.configuration.BrowserSettings;
import com.cameo.factory.WebDriverFactory;
import com.cameo.test.base.JoinAndLogin;
import org.openqa.selenium.WebDriver;

public class TestJoinAndLoginInFirefox extends JoinAndLogin {

    protected WebDriver getDriver() {
        System.setProperty("webdriver."+BrowserSettings.WEBDRIVER_GECKO_DRIVER+".driver", BrowserSettings.GECKO_DRIVER_LOCATION);
        browserType = BrowserSettings.FIREFOX_BROWSER_TYPE;
        return WebDriverFactory.getWebDriver(WebDriverFactory.WebDrivers.FIREFOX_DRIVER);
    }
}


