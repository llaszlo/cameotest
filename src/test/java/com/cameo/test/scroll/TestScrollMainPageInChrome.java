package com.cameo.test.scroll;

import com.cameo.configuration.BrowserSettings;
import com.cameo.factory.WebDriverFactory;
import com.cameo.test.base.ScrollMainPage;
import org.openqa.selenium.WebDriver;

public class TestScrollMainPageInChrome extends ScrollMainPage {

    protected WebDriver getDriver() {
        System.setProperty("webdriver."+BrowserSettings.WEBDRIVER_CHROME_DRIVER+".driver", BrowserSettings.CHROME_DRIVER_LOCATION);
        return WebDriverFactory.getWebDriver(WebDriverFactory.WebDrivers.CHROME_DRIVER);
    }

}