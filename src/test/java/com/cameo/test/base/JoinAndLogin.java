package com.cameo.test.base;
/**
 * abstract parent class
 * this contains 3 test annotations:
 * - TestCaseJoinAndLogInAndDeleteAccount: This test check whether a user can register on the "https://vimeo.com/cameo" webpage and after it the user can login with this registration.
 * - TestCaseDoubleJoinIsAllowance: This test also check if a user has already registered he/she can't register with the same email address.
 * - TestCaseUnregisteredUserCanLogin: This test check if a user is not registered he/she can login anyway
 */


import com.cameo.helper.DeleteAccount;
import com.cameo.helper.Join;
import com.cameo.helper.LogIn;
import com.cameo.helper.LogOut;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.cameo.helper.ThreadSleepSupport.sleep;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class JoinAndLogin {
    private static final String NO_THANKS = "No thanks »";

    protected static WebDriver driver;

    private WebDriverWait wait;
    protected String browserType;
    private Join join;
    private LogOut logOut;
    private LogIn logIn;
    private DeleteAccount deleteAccount;


    public JoinAndLogin() {
        JoinAndLogin.driver = getDriver();
        this.wait = new WebDriverWait(driver, 6);
        this.driver.manage().window().maximize();
        this.join = new Join(driver, wait, browserType);
        this.logOut = new LogOut(driver, wait);
        this.logIn = new LogIn(driver, wait, browserType);
        this.deleteAccount = new DeleteAccount(driver, wait, browserType);
    }

    protected abstract WebDriver getDriver();


    @Before
    public void setUpURL() {

        driver.get("https://vimeo.com/cameo");

    }


    @Test
    public void testCaseJoinAndLogInAndDeleteAccount() {

        join.join();
        join.joinIsPossible();
        logOut.logOut();
        logIn.logIn();
        deleteAccount.deleteAccount();

    }


    @Test
    public void testCaseDoubleJoinIsAllowance() {

        // CHECK IF DOUBLE JOIN IS POSSIBLE WITH THE SAME EMAIL ADDRESS
        join.join();
        join.joinIsPossible();
        logOut.logOut();
        join.join();
        sleep(2000);
        assertFalse(driver.findElements(By.xpath("//a[contains(.,'Want to log in?')]")).size() == 0);
        assertTrue(driver.findElements(By.linkText("No thanks »")).isEmpty());
        logIn.logIn();
        deleteAccount.deleteAccount();
    }


    @Test
    public void testCaseUnregisteredUserCanLogin() {

        // CHECK IF AN UNREGISTERED USER CAN LOGIN
        join.join();
        join.joinIsPossible();
        deleteAccount.deleteAccount();
        sleep(1500);  // in automation test despite the account was deleted the home page was still enabled because of the rapid navigate() method
        logIn.logIn();
        sleep(2000);
        assertFalse(driver.findElements(By.xpath("//div[contains(.,'Email and password do not match')]")).size() == 0);
        assertTrue(driver.findElements(By.xpath("//div[@id='topnav_menu_avatar']/a/img")).isEmpty());
    }


    @After
    public void closeBrowser() {

        driver.close();

    }

    @AfterClass
    public static void tearDownOnce() {

        driver.quit();
    }
}