package com.cameo.test.base;
/**
 * abstract parernt class
 * this contains 8 test annotations. Each of them test 1 section on the main page.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.cameo.helper.IsWebElementVisible.isVisible;
import static com.cameo.helper.IsWebElementVisible.isVisibleArrayList;
import static com.cameo.helper.IsWebElementChangeInSize.isShrink;
import static com.cameo.helper.IsWebElementChangeInSize.isEnlarge;
import static com.cameo.helper.ThreadSleepSupport.sleep;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class ScrollMainPage{
    protected static WebDriver driver;
    private Actions action;
    private WebDriverWait wait;
    private JavascriptExecutor jsExec;
    private int browserWidthInMax;
    private int browserHeightInMax;
    private String assertMessage;

    public ScrollMainPage() {
        ScrollMainPage.driver = getDriver();
        this.action = new Actions(driver);
        this.wait = new WebDriverWait(driver, 6);
        this.jsExec = (JavascriptExecutor)driver;
        this.driver.manage().window().setPosition(new Point(0,0));
        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    protected abstract WebDriver getDriver();


    @Before
    public void setUpURL() {
        driver.manage().window().maximize();
        browserWidthInMax = driver.manage().window().getSize().getWidth();
        browserHeightInMax = driver.manage().window().getSize().getHeight();
        driver.get("https://vimeo.com/cameo");
    }


    @Test
    public void testCaseIntro_Phone() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{900,500},{767,500},{625,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement sectionIntro = driver.findElement(By.id("intro"));
            WebElement sectionPhone = driver.findElement(By.id("phone"));
            WebElement sectionFeatures = driver.findElement(By.id("features"));

            jsExec.executeScript("arguments[0].scrollIntoView()", sectionIntro);
            sleep(5000);
            wait.until(ExpectedConditions.visibilityOf(sectionIntro));

            WebElement sectionIntroTopNavigationBar = driver.findElement(By.xpath("//div[@class='topnav_desktop ']"));
            if (sectionIntroTopNavigationBar.isDisplayed()) {

                //INTRO SECTION ELEMENTS
                WebElement sectionIntroBackground = driver.findElement(By.xpath("//div[@class='intro_background']"));
                WebElement sectionIntroImage = driver.findElement(By.xpath("//img[@class='intro_image']"));
                WebElement sectionIntroCameoLogo = driver.findElement(By.xpath("//div[@class='logo']"));
                WebElement sectionIntroDownload = driver.findElement(By.xpath("//span[contains(.,'Download Cameo')]"));
                WebElement sectionIntroHome = driver.findElement(By.xpath("//img[@class='intro_ui home']"));
                WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));

                //PHONE SECTION ELEMENTS
                WebElement sectionPhoneHeader = driver.findElement(By.xpath("//header[contains(.,'Small Screen, Cinematic ResultsTurn your video clips into gorgeous short films in just a few swipes. All you need is an iPhone and the free Cameo app.')]"));
                WebElement sectionPhonePhonePicture = driver.findElement(By.xpath("//div[@class='phone_picture animated']"));
                WebElement sectionPhoneFarLeftPic = driver.findElement(By.xpath("//div[@class='picture left far sliding_picture1']"));
                WebElement sectionPhoneNearLeftPic = driver.findElement(By.xpath("//div[@class='picture left near sliding_picture2']"));
                WebElement sectionPhoneNearRightPic = driver.findElement(By.xpath("//div[@class='picture right near sliding_picture3']"));
                WebElement sectionPhoneFarRightPic = driver.findElement(By.xpath("//div[@class='picture right far sliding_picture4']"));
                WebElement sectionPhoneVideoControl = driver.findElement(By.xpath("//div[@class='video_controls']"));

                //LOOP
                for (int i = 20; i <= sectionFeatures.getLocation().getY() - 300; i += 10) {

                    int xOfIntroImageBefore = sectionIntroImage.getLocation().getX();
                    int yOfIntroImageBefore = sectionIntroImage.getLocation().getY();

                    jsExec.executeScript("window.scrollTo(0," + i + ");");
                    sleep(300);

                    String sOpacitySectionMenu = sectionIntroSectionMenu.getCssValue("opacity");
                    String sOpacityIntroBackgroung = sectionIntroBackground.getCssValue("opacity");
                    String sOpacityPhoneHeader = sectionPhoneHeader.getCssValue("opacity");
                    double dOpacitySectionMenu = Double.parseDouble(sOpacitySectionMenu);
                    double dOpacityIntroBackgroung = Double.parseDouble(sOpacityIntroBackgroung);
                    double dOpacityPhoneHeader = Double.parseDouble(sOpacityPhoneHeader);

                    int xOfIntroImageAfter = sectionIntroImage.getLocation().getX();
                    int yOfIntroImageAfter = sectionIntroImage.getLocation().getY();

                    if (Math.abs(dOpacitySectionMenu) <= 0.000001 && Math.abs(1.0 - dOpacityIntroBackgroung) <= 0.000001) {
                        assertTrue(assertMessage,isVisible(sectionIntroTopNavigationBar));
                        assertTrue(assertMessage,isVisible(sectionIntroBackground));
                        assertTrue(assertMessage,isVisible(sectionIntroImage));
                        assertTrue(assertMessage,isVisible(sectionIntroCameoLogo));
                        assertTrue(assertMessage,isVisible(sectionIntroDownload));
                        assertFalse(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(assertMessage,isShrink(xOfIntroImageBefore, yOfIntroImageBefore, xOfIntroImageAfter, yOfIntroImageAfter));
                    } else if (Math.abs(dOpacitySectionMenu) > 0.0 && Math.abs(1.0 - dOpacityIntroBackgroung) <= 0.000001) {
                        assertFalse(assertMessage,isVisible(sectionIntroTopNavigationBar));
                        assertTrue(assertMessage,isVisible(sectionIntroBackground));
                        assertTrue(assertMessage,isVisible(sectionIntroImage));
                        assertFalse(assertMessage,isVisible(sectionIntroCameoLogo));
                        assertFalse(assertMessage,isVisible(sectionIntroDownload));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(assertMessage,isShrink(xOfIntroImageBefore, yOfIntroImageBefore, xOfIntroImageAfter, yOfIntroImageAfter));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Editing')]")).getAttribute("class").contains("active"));
                    } else if (Math.abs(dOpacitySectionMenu) > 0.0 && (Math.abs(dOpacityIntroBackgroung) < 1.0 && Math.abs(dOpacityIntroBackgroung) > 0.0)) {
                        assertFalse(assertMessage,isVisible(sectionIntroTopNavigationBar));
                        assertTrue(assertMessage,isVisible(sectionIntroBackground));
                        assertTrue(assertMessage,isVisible(sectionIntroImage));
                        assertFalse(assertMessage,isVisible(sectionIntroCameoLogo));
                        assertFalse(assertMessage,isVisible(sectionIntroDownload));
                        assertTrue(assertMessage,isVisible(sectionIntroHome));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Editing')]")).getAttribute("class").contains("active"));
                    } else if (Math.abs(dOpacityIntroBackgroung) <= 0.000001 && Math.abs(dOpacityPhoneHeader) <= 0.000001) {
                        assertFalse(assertMessage,isVisible(sectionIntroTopNavigationBar));
                        assertFalse(assertMessage,isVisible(sectionIntroBackground));
                        assertFalse(assertMessage,isVisible(sectionIntroImage));
                        assertFalse(assertMessage,isVisible(sectionIntroCameoLogo));
                        assertFalse(assertMessage,isVisible(sectionIntroDownload));
                        assertFalse(assertMessage,isVisible(sectionIntroHome));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Editing')]")).getAttribute("class").contains("active"));
                    } else if (Math.abs(dOpacityPhoneHeader) > 0.0 && Math.abs(dOpacityPhoneHeader) < 1.0) {
                        assertTrue(assertMessage,isVisible(sectionPhoneHeader));
                        assertTrue(assertMessage,isVisible(sectionPhonePhonePicture));
                        assertFalse(assertMessage,isVisible(sectionPhoneFarLeftPic));
                        assertFalse(assertMessage,isVisible(sectionPhoneNearLeftPic));
                        assertFalse(assertMessage,isVisible(sectionPhoneNearRightPic));
                        assertFalse(assertMessage,isVisible(sectionPhoneFarRightPic));
                        assertFalse(assertMessage,isVisible(sectionPhoneVideoControl));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Editing')]")).getAttribute("class").contains("active"));
                    } else {
                        wait.until(ExpectedConditions.visibilityOf(sectionPhoneVideoControl));
                        assertTrue(assertMessage,isVisible(sectionPhoneHeader));
                        assertTrue(assertMessage,isVisible(sectionPhonePhonePicture));
                        if (driver.manage().window().getSize().getWidth() > 1150) {
                            assertTrue(assertMessage,isVisible(sectionPhoneFarLeftPic));
                            assertTrue(assertMessage,isVisible(sectionPhoneFarRightPic));
                        }
                        assertTrue(assertMessage,isVisible(sectionPhoneNearLeftPic));
                        assertTrue(assertMessage,isVisible(sectionPhoneNearRightPic));
                        assertTrue(assertMessage,isVisible(sectionPhoneVideoControl));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Editing')]")).getAttribute("class").contains("active"));
                    }

                }
            } else {
                //SMALL SCREEN ELEMENTS
                WebElement sectionTovNav = driver.findElement(By.xpath("//button[contains(@class,'topnav_mobile_button topnav_mobile_pull_left topnav_mobile_header_logo js-topnav_mobile_header_logo topnav_icon_mobile_vlogo_b')]"));

                WebElement sectionTovNavVIcon = driver.findElement(By.xpath("//a[contains(@href,'//vimeo.com/search')]"));
                WebElement sectionTovNavSearchIcon = driver.findElement(By.xpath("//nav[@id='topnav_mobile']/div"));

                WebElement sectionIntroCameoLogo = driver.findElement(By.xpath("//div[@class='logo']"));
                WebElement sectionIntroTagLine = driver.findElement(By.xpath("//div[contains(@class,'tagline')]"));
                WebElement sectionIntroDownloadCameoLink = driver.findElement(By.xpath("//span[contains(.,'Download Cameo')]"));

                WebElement sectionPhoneH2SmallScreen = driver.findElement(By.xpath("//h2[contains(.,'Small Screen, Cinematic Results')]"));
                WebElement sectionPhonePsection = driver.findElement(By.xpath("//p[contains(.,'Turn your video clips into gorgeous short films in just a few swipes. All you need is an iPhone and the free Cameo app.')]"));
                WebElement sectionPhonePhonePicture = driver.findElement(By.xpath("//div[contains(@class,'picture static')]"));
                WebElement sectionPhoneNearLeftPic = driver.findElement(By.xpath("//div[contains(@class,'picture left near sliding_picture2')]"));
                WebElement sectionPhoneNearRightPic = driver.findElement(By.xpath("//div[contains(@class,'picture right near sliding_picture3')]"));
                WebElement sectionPhoneFarLeftPic = driver.findElement(By.xpath("//div[contains(@class,'picture left far sliding_picture1')]"));
                WebElement sectionPhoneFarRightPic = driver.findElement(By.xpath("//div[contains(@class,'picture right far sliding_picture4')]"));
                WebElement sectionPhoneVideoControl = driver.findElement(By.xpath("//div[contains(@class,'controls')]"));

                jsExec.executeScript("arguments[0].scrollIntoView()", sectionTovNav);
                sleep(500);
                assertTrue(assertMessage,isVisible(sectionTovNavVIcon));
                assertTrue(assertMessage,isVisible(sectionTovNavSearchIcon));
                jsExec.executeScript("arguments[0].scrollIntoView()", sectionIntro);
                assertTrue(assertMessage,isVisible(sectionIntroCameoLogo));
                assertTrue(assertMessage,isVisible(sectionIntroTagLine));
                assertTrue(assertMessage,isVisible(sectionIntroDownloadCameoLink));
                jsExec.executeScript("arguments[0].scrollIntoView()", sectionPhone);
                assertTrue(assertMessage,isVisible(sectionPhoneH2SmallScreen));
                assertTrue(assertMessage,isVisible(sectionPhonePsection));
                assertTrue(assertMessage,isVisible(sectionPhonePhonePicture));
                assertTrue(assertMessage,isVisible(sectionPhoneNearLeftPic));
                assertTrue(assertMessage,isVisible(sectionPhoneNearRightPic));
                if (driver.manage().window().getSize().getWidth() > 630) {
                    assertTrue(assertMessage,isVisible(sectionPhoneFarLeftPic));
                    assertTrue(assertMessage,isVisible(sectionPhoneFarRightPic));
                }
                assertTrue(assertMessage,isVisible(sectionPhoneVideoControl));

            }
        }
    }

    @Test
    public void testCaseFeature() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{767,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement sectionFeatures = driver.findElement(By.id("features"));
            WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));
            jsExec.executeScript("arguments[0].scrollIntoView()", sectionFeatures);
            sleep(1000);
            if (sectionIntroSectionMenu.isDisplayed()) {

                //FEATURES ELEMENTS
                WebElement sectionFeaturesWrapper = driver.findElement(By.xpath("//div[@class='features_wrapper']"));
                WebElement sectionFeaturesLeftPic = driver.findElement(By.xpath("//div[@class='picture feature_picture1']"));
                WebElement sectionFeaturesCentralPic = driver.findElement(By.xpath("//div[@class='picture feature_picture2']"));
                WebElement sectionFeaturesRightPic = driver.findElement(By.xpath("//div[@class='picture feature_picture3']"));

                wait.until(ExpectedConditions.visibilityOf(sectionFeaturesRightPic));

                assertTrue(assertMessage,isVisible(sectionFeaturesWrapper));
                assertTrue(assertMessage,isVisible(sectionFeaturesLeftPic));
                assertTrue(assertMessage,isVisible(sectionFeaturesCentralPic));
                assertTrue(assertMessage,isVisible(sectionFeaturesRightPic));
                assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                assertTrue(driver.findElement(By.xpath("//a[contains(.,'Editing')]")).getAttribute("class").contains("active"));
            } else {
                //SMALL SCREEN ELEMENTS
                WebElement sectionFeaturesPicture = driver.findElement(By.xpath("//div[contains(@class,'picture feature_picture1')]"));
                WebElement sectionFeaturesEnjoyH = driver.findElement(By.xpath("//h3[contains(.,'Enjoyable editing')]"));
                WebElement sectionFeaturesMakeGorgeousP = driver.findElement(By.xpath("//p[contains(.,'Make gorgeous HD videos with powerful editing software')]"));
                WebElement sectionFeaturesRightPic = driver.findElement(By.xpath("//div[@class='picture feature_picture3']"));

                assertTrue(assertMessage,isVisible(sectionFeaturesPicture));
                assertTrue(assertMessage,isVisible(sectionFeaturesEnjoyH));
                assertTrue(assertMessage,isVisible(sectionFeaturesMakeGorgeousP));
                assertFalse(assertMessage,isVisible(sectionFeaturesRightPic));
            }
        }
    }



    @Test
    public void testCaseThemes() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{767,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement sectionThemes = driver.findElement(By.id("themes"));
            WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));
            jsExec.executeScript("arguments[0].scrollIntoView()", sectionThemes);
            sleep(1000);
            if (sectionIntroSectionMenu.isDisplayed()) {

                //THEMES ELEMENTS
                WebElement sectionThemesHeader = driver.findElement(By.xpath("//header[contains(.,'From Camera Roll To Red CarpetAdd cinematic flair')]"));
                WebElement sectionThemesNavigation = driver.findElement(By.xpath("//div[@class='navigation']"));
                WebElement sectionThemesYosemitaWeekend = driver.findElement(By.xpath("//section[@id='themes']/div[2]/div/div/div"));
                WebElement sectionThemesVideo = driver.findElement(By.xpath("//section[@id='themes']/div[2]/div/div[2]/video"));

                wait.until(ExpectedConditions.visibilityOf(sectionThemesHeader));

                assertTrue(assertMessage,isVisible(sectionThemesHeader));
                assertTrue(assertMessage,isVisible(sectionThemesNavigation));
                assertTrue(assertMessage,isVisible(sectionThemesYosemitaWeekend));
                assertTrue(assertMessage,isVisible(sectionThemesVideo));
                assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                assertTrue(driver.findElement(By.xpath("//a[contains(.,'Themes')]")).getAttribute("class").contains("active"));
            } else {
                //SMALL SCREEN ELEMENTS
                WebElement sectionThemesFromCameraH2 = driver.findElement(By.xpath("//h2[contains(.,'From Camera Roll To Red Carpet')]"));
                WebElement sectionThemesWhiteP = driver.findElement(By.xpath("//p[contains(@class,'white')]"));
                WebElement sectionThemesYosemitaWeekend = driver.findElement(By.xpath("//section[@id='themes']/div[2]/div/div/div"));
                WebElement sectionThemesNavigation = driver.findElement(By.xpath("//div[@class='navigation']"));

                assertTrue(assertMessage,isVisible(sectionThemesFromCameraH2));
                assertTrue(assertMessage,isVisible(sectionThemesWhiteP));
                assertTrue(assertMessage,isVisible(sectionThemesYosemitaWeekend));
                assertFalse(assertMessage,isVisible(sectionThemesNavigation));
            }
        }
    }


    @Test
    public void testCaseSoundTracks() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{767,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement sectionSoundtracks = driver.findElement(By.id("soundtracks"));
            WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));
            jsExec.executeScript("arguments[0].scrollIntoView()", sectionSoundtracks);
            sleep(1000);
            if (sectionIntroSectionMenu.isDisplayed()) {

                //SOUND TRACKS ELEMENTS
                WebElement sectionSoundtracksHeader = driver.findElement(By.xpath("//header[contains(.,'Soundtrack MagicFind your perfect soundtrack in our carefully curated music catalog.')]"));
                WebElement sectionSoundtracksCentralPics = driver.findElement(By.xpath("//div[@class='group featured']"));
                WebElement sectionSoundtracksLeftPics = driver.findElement(By.xpath("//div[@class='group left']"));
                WebElement sectionSoundtracksRightPics = driver.findElement(By.xpath("//div[@class='group right']"));

                sleep(5000);
                wait.until(ExpectedConditions.visibilityOf(sectionSoundtracksHeader));

                assertTrue(assertMessage,isVisible(sectionSoundtracksHeader));
                assertTrue(assertMessage,isVisible(sectionSoundtracksCentralPics));
                assertTrue(assertMessage,isVisible(sectionSoundtracksLeftPics));
                assertTrue(assertMessage,isVisible(sectionSoundtracksRightPics));
                assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                assertTrue(driver.findElement(By.xpath("//a[contains(.,'Soundtracks')]")).getAttribute("class").contains("active"));

                //CHECK PLAYING MUSIC
                WebElement sectionSoundtracksFirstPlayIcon1 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div/a/div/div"));
                action.click(sectionSoundtracksFirstPlayIcon1).perform();
                wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//div[@class='group featured']/a[contains(@class,'soundtrack1 current is_playing')]"))));

                WebElement sectionSoundtracksFirstPlayIcon2 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div/a[2]/div/div"));
                action.click(sectionSoundtracksFirstPlayIcon2).perform();
                wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//div[@class='group featured']/a[contains(@class,'soundtrack2 current is_playing')]"))));

                WebElement sectionSoundtracksFirstPlayIcon3 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div/a[3]/div/div"));
                action.click(sectionSoundtracksFirstPlayIcon3).perform();
                wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//div[@class='group featured']/a[contains(@class,'soundtrack3 current is_playing')]"))));

                WebElement sectionSoundtracksFirstPlayIcon4 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div/a[4]/div/div"));
                action.click(sectionSoundtracksFirstPlayIcon4).perform();
                wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//img[contains(@src,'playing.gif')]"))));
                assertTrue(assertMessage,isVisible(driver.findElement(By.xpath("//div[@class='group featured']/a[contains(@class,'soundtrack4 current is_playing')]"))));
                action.click(sectionSoundtracksFirstPlayIcon4).perform();


                //CHECK WHETHER THE CENTRAL 4 PICTURES ARE ENLARGING AND SHRINKING
                jsExec.executeScript("arguments[0].scrollIntoView()", sectionSoundtracksLeftPics);

                sleep(1500);
                int xOfSoundtracksCentralPicsBefore = sectionSoundtracksCentralPics.getLocation().getX();
                int yOfSoundtracksCentralPicsBefore = sectionSoundtracksCentralPics.getLocation().getY();

                for (int i = 20; i <= 800; i += 20) {
                    int yPosition = sectionSoundtracksLeftPics.getLocation().getY() - i;
                    jsExec.executeScript("window.scrollTo(0," + yPosition + ");");

                    if (!(sectionSoundtracks.getAttribute("class").contains("seen"))) {
                        sleep(1500);
                        int xOfSoundtracksCentralPicsAfter = sectionSoundtracksCentralPics.getLocation().getX();
                        int yOfSoundtracksCentralPicsAfter = sectionSoundtracksCentralPics.getLocation().getY();

                        assertTrue(assertMessage,isShrink(xOfSoundtracksCentralPicsBefore, yOfSoundtracksCentralPicsBefore, xOfSoundtracksCentralPicsAfter, yOfSoundtracksCentralPicsAfter));

                        yPosition = yPosition + 40;
                        jsExec.executeScript("window.scrollTo(0," + yPosition + ");");

                        sleep(1500);
                        xOfSoundtracksCentralPicsBefore = sectionSoundtracksCentralPics.getLocation().getX();
                        yOfSoundtracksCentralPicsBefore = sectionSoundtracksCentralPics.getLocation().getY();

                        assertTrue(assertMessage,isEnlarge(xOfSoundtracksCentralPicsAfter, yOfSoundtracksCentralPicsAfter, xOfSoundtracksCentralPicsBefore, yOfSoundtracksCentralPicsBefore));
                        break;
                    }

                }
            } else {
                //SMALL SCREEN ELEMENTS
                WebElement sectionSoundtracksHeader = driver.findElement(By.xpath("//h2[contains(.,'Soundtrack Magic')]"));
                WebElement sectionSoundtracksP = driver.findElement(By.xpath("//p[contains(.,'Find your perfect soundtrack in our carefully curated music catalog.')]"));
                WebElement sectionSoundtracksPic2 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[2]"));
                WebElement sectionSoundtracksPic3 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[3]"));
                WebElement sectionSoundtracksPic4 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[4]"));
                WebElement sectionSoundtracksPic5 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[5]"));
                WebElement sectionSoundtracksPic8 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[8]"));
                WebElement sectionSoundtracksPic9 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[9]"));
                WebElement sectionSoundtracksPic10 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[10]"));
                WebElement sectionSoundtracksPic11 = driver.findElement(By.xpath("//section[@id='soundtracks']/div/div[4]/div[11]"));
                WebElement sectionSoundtracksRightPics = driver.findElement(By.xpath("//div[@class='group right']"));

                assertTrue(assertMessage,isVisible(sectionSoundtracksHeader));
                assertTrue(assertMessage,isVisible(sectionSoundtracksP));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic2));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic3));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic4));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic5));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic8));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic9));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic10));
                assertTrue(assertMessage,isVisible(sectionSoundtracksPic11));
                assertFalse(assertMessage,isVisible(sectionSoundtracksRightPics));
            }
        }
    }



    @Test
    public void testCaseVideos() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{767,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement sectionVideos = driver.findElement(By.id("videos"));
            WebElement sectionCreators = driver.findElement(By.id("creators"));
            WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));
            jsExec.executeScript("arguments[0].scrollIntoView()", sectionVideos);
            sleep(1000);
            if (sectionIntroSectionMenu.isDisplayed()) {

                //VIDEOS ELEMENTS
                WebElement sectionVideosHeader = driver.findElement(By.xpath("//header[contains(.,'Get your daily dose of inspiration from our Best of Cameo channel, where we showcase some of the most stunning videos created with the app.')]"));
                WebElement sectionVideosHeaderBadge = driver.findElement(By.xpath("//div[contains(@class,'header_badge badge_best_of_cameo')]"));
                WebElement sectionVideosDIVofUpLeftPic = driver.findElement(By.xpath("//section[@id='videos']/div/div/div"));
                WebElement sectionVideosUpLeftPic = driver.findElement(By.xpath("//div[@class='poster video_picture1']"));
                WebElement sectionVideosDIVofUpRightPic = driver.findElement(By.xpath("//section[@id='videos']/div/div/div[2]"));
                WebElement sectionVideosUpRightPic = driver.findElement(By.xpath("//div[@class='poster video_picture2']"));
                WebElement sectionVideosDownDIVofLeftPic = driver.findElement(By.xpath("//section[@id='videos']/div/div/div[3]"));
                WebElement sectionVideosDownLeftPic = driver.findElement(By.xpath("//div[@class='poster video_picture3']"));
                WebElement sectionVideosDIVofDownRightPic = driver.findElement(By.xpath("//section[@id='videos']/div/div/div[4]"));
                WebElement sectionVideosDownRightPic = driver.findElement(By.xpath("//div[@class='poster video_picture4']"));
                WebElement sectionVideosWatchMoreLink = driver.findElement(By.xpath("//a[contains(@class,'button more_link')]"));

                wait.until(ExpectedConditions.visibilityOf(sectionVideosHeader));

                for (int i = sectionVideos.getLocation().getY(); i <= sectionCreators.getLocation().getY(); i += 50) {
                    jsExec.executeScript("window.scrollTo(0," + i + ");");

                    if (!(sectionVideosDIVofUpLeftPic.getAttribute("class").contains("seen") && sectionVideosDIVofUpRightPic.getAttribute("class").contains("seen"))) {
                        assertTrue(assertMessage,isVisible(sectionVideosHeaderBadge));
                        assertFalse(assertMessage,isVisible(sectionVideosDownLeftPic));
                        assertFalse(assertMessage,isVisible(sectionVideosDownRightPic));
                        assertFalse(assertMessage,isVisible(sectionVideosWatchMoreLink));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                    } else if ((sectionVideosDIVofUpLeftPic.getAttribute("class").contains("seen") && sectionVideosDIVofUpRightPic.getAttribute("class").contains("seen")) && (!(sectionVideosDownDIVofLeftPic.getAttribute("class").contains("seen") && sectionVideosDIVofDownRightPic.getAttribute("class").contains("seen")))) {
                        wait.until(ExpectedConditions.visibilityOf(sectionVideosUpRightPic));
                        assertTrue(assertMessage,isVisible(sectionVideosHeaderBadge));
                        assertTrue(assertMessage,isVisible(sectionVideosUpLeftPic));
                        assertTrue(assertMessage,isVisible(sectionVideosUpRightPic));
                        assertFalse(assertMessage,isVisible(sectionVideosWatchMoreLink));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                    } else if ((sectionVideosDownDIVofLeftPic.getAttribute("class").contains("seen") && sectionVideosDIVofDownRightPic.getAttribute("class").contains("seen")) && !sectionVideosWatchMoreLink.getAttribute("class").contains("seen")) {
                        wait.until(ExpectedConditions.visibilityOf(sectionVideosDownRightPic));
                        assertTrue(assertMessage,isVisible(sectionVideosHeaderBadge));
                        assertTrue(assertMessage,isVisible(sectionVideosUpLeftPic));
                        assertTrue(assertMessage,isVisible(sectionVideosUpRightPic));
                        assertTrue(assertMessage,isVisible(sectionVideosDownLeftPic));
                        assertTrue(assertMessage,isVisible(sectionVideosDownRightPic));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                    } else {
                        wait.until(ExpectedConditions.visibilityOf(sectionVideosWatchMoreLink));
                        assertTrue(assertMessage,isVisible(sectionVideosHeaderBadge));
                        assertTrue(assertMessage,isVisible(sectionVideosUpLeftPic));
                        assertTrue(assertMessage,isVisible(sectionVideosUpRightPic));
                        assertTrue(assertMessage,isVisible(sectionVideosDownLeftPic));
                        assertTrue(assertMessage,isVisible(sectionVideosDownRightPic));
                        assertTrue(assertMessage,isVisible(sectionVideosWatchMoreLink));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                        break;
                    }
                }
            } else {
                //SMALL SCREEN ELEMENTS
                WebElement sectionVideosHeader = driver.findElement(By.xpath("//div[contains(@class,'header_badge badge_best_of_cameo seen')]"));
                WebElement sectionVideosP = driver.findElement(By.xpath("//p[contains(.,'Get your daily dose of inspiration from our Best of Cameo channel')]"));
                WebElement sectionVideosPic1 = driver.findElement(By.xpath("//div[contains(@class,'poster video_picture1')]"));
                WebElement sectionVideosPic2 = driver.findElement(By.xpath("//div[contains(@class,'poster video_picture2')]"));
                WebElement sectionVideosPic3 = driver.findElement(By.xpath("//div[contains(@class,'poster video_picture3')]"));
                WebElement sectionVideosPic4 = driver.findElement(By.xpath("//div[contains(@class,'poster video_picture4')]"));
                WebElement sectionVideosWatchMoreLinkMobile = driver.findElement(By.xpath("//a[contains(@class,'link mobile')]"));
                WebElement sectionVideosWatchMoreLink = driver.findElement(By.xpath("//a[contains(@class,'button more_link')]"));

                assertTrue(assertMessage,isVisible(sectionVideosHeader));
                assertTrue(assertMessage,isVisible(sectionVideosP));
                assertTrue(assertMessage,isVisible(sectionVideosPic1));
                assertTrue(assertMessage,isVisible(sectionVideosPic2));
                assertTrue(assertMessage,isVisible(sectionVideosPic3));
                assertTrue(assertMessage,isVisible(sectionVideosPic4));
                assertTrue(assertMessage,isVisible(sectionVideosWatchMoreLinkMobile));
                assertFalse(assertMessage,isVisible(sectionVideosWatchMoreLink));
            }
        }
    }



    @Test
    public void testCaseCreators() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{900,500},{767,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement sectionCreators = driver.findElement(By.id("creators"));
            WebElement sectionDownload = driver.findElement(By.id("download"));
            WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));
            jsExec.executeScript("arguments[0].scrollIntoView()", sectionCreators);
            sleep(1000);
            if (sectionIntroSectionMenu.isDisplayed()) {

                //CREATORS ELEMENTS
                WebElement sectionCreatorsHeader = driver.findElement(By.xpath("//header[contains(.,'A Community of CreatorsJoin our group to see how others are using Cameo to make beautiful videos and short films, and share your own.')]"));

                WebElement sectionCreatorsFirstRow = driver.findElement(By.xpath("//section[@id='creators']/div/div/div/div/ul/div"));
                WebElement sectionCreatorsSecondRow = driver.findElement(By.xpath("//section[@id='creators']/div/div/div/div/ul/div[2]"));

                List<WebElement> sectionCreators1FirstRow = new ArrayList<WebElement>();
                List<WebElement> sectionCreators1SecondRow = new ArrayList<WebElement>();
                sectionCreators1FirstRow.add(0, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/602737860_640x360.jpg?r=pad']")));
                sectionCreators1FirstRow.add(1, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/577587759_640x360.jpg?r=pad']")));
                sectionCreators1FirstRow.add(2, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/577279824_640x360.jpg?r=pad']")));
                sectionCreators1FirstRow.add(3, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/577275610_640x360.jpg?r=pad']")));
                sectionCreators1SecondRow.add(0, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/577077610_640x360.jpg?r=pad']")));
                sectionCreators1SecondRow.add(1, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573595927_640x360.jpg?r=pad']")));
                sectionCreators1SecondRow.add(2, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573592605_640x360.jpg?r=pad']")));
                sectionCreators1SecondRow.add(3, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573589830_640x360.jpg?r=pad']")));

                List<WebElement> sectionCreators2 = new ArrayList<WebElement>();
                sectionCreators2.add(0, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573588913_640x360.jpg?r=pad']")));
                sectionCreators2.add(1, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573588763_640x360.jpg?r=pad']")));
                sectionCreators2.add(2, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573583049_640x360.jpg?r=pad']")));
                sectionCreators2.add(3, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573581253_640x360.jpg?r=pad']")));
                sectionCreators2.add(4, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573580966_640x360.jpg?r=pad']")));
                sectionCreators2.add(5, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573576865_640x360.jpg?r=pad']")));
                sectionCreators2.add(6, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573593275_640x360.jpg?r=pad']")));
                sectionCreators2.add(7, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573575389_640x360.jpg?r=pad']")));

                List<WebElement> sectionCreators3 = new ArrayList<WebElement>();
                sectionCreators3.add(0, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573574005_640x360.jpg?r=pad']")));
                sectionCreators3.add(1, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573573792_640x360.jpg?r=pad']")));
                sectionCreators3.add(2, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573573569_640x360.jpg?r=pad']")));
                sectionCreators3.add(3, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573571846_640x360.jpg?r=pad']")));
                sectionCreators3.add(4, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573571347_640x360.jpg?r=pad']")));
                sectionCreators3.add(5, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573570696_640x360.jpg?r=pad']")));
                sectionCreators3.add(6, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573569295_640x360.jpg?r=pad']")));
                sectionCreators3.add(7, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573528994_640x360.jpg?r=pad']")));

                List<WebElement> sectionCreators4 = new ArrayList<WebElement>();
                sectionCreators4.add(0, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573528272_640x360.jpg?r=pad']")));
                sectionCreators4.add(1, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/573285386_640x360.jpg?r=pad']")));
                sectionCreators4.add(2, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/572914150_640x360.jpg?r=pad']")));
                sectionCreators4.add(3, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/571050582_640x360.jpg?r=pad']")));
                sectionCreators4.add(4, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/570918925_640x360.jpg?r=pad']")));
                sectionCreators4.add(5, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/570533570_640x360.jpg?r=pad']")));
                sectionCreators4.add(6, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/569473175_640x360.jpg?r=pad']")));
                sectionCreators4.add(7, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/568400801_640x360.jpg?r=pad']")));

                List<WebElement> sectionCreators5 = new ArrayList<WebElement>();
                sectionCreators5.add(0, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/568382472_640x360.jpg?r=pad']")));
                sectionCreators5.add(1, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/567709130_640x360.jpg?r=pad']")));
                sectionCreators5.add(2, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/566326626_640x360.jpg?r=pad']")));
                sectionCreators5.add(3, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/565469785_640x360.jpg?r=pad']")));
                sectionCreators5.add(4, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/564165666_640x360.jpg?r=pad']")));
                sectionCreators5.add(5, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/563840064_640x360.jpg?r=pad']")));
                sectionCreators5.add(6, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/563697855_640x360.jpg?r=pad']")));
                sectionCreators5.add(7, driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/562647903_640x360.jpg?r=pad']")));

                WebElement sectionCreatorsNavigationLeft = driver.findElement(By.xpath("//a[contains(@class,'navigation left')]"));
                WebElement sectionCreatorsNavigationRight = driver.findElement(By.xpath("//a[contains(@class,'navigation right')]"));

                WebElement sectionCreatorsExporeMore = driver.findElement(By.xpath("//a[@href='/groups/cameo']"));

                WebElement sectionCreatorsCommunitySocial = driver.findElement(By.xpath("//div[contains(@class,'social')]"));
                WebElement sectionCreatorsCommunityFacebook = driver.findElement(By.xpath("//div[@class='icon icon_facebook']"));
                WebElement sectionCreatorsCommunityTwitter = driver.findElement(By.xpath("//div[@class='icon icon_twitter']"));
                WebElement sectionCreatorsCommunityInstagram = driver.findElement(By.xpath("//div[@class='icon icon_instagram']"));


                sleep(5000);
                wait.until(ExpectedConditions.visibilityOf(sectionCreatorsHeader));

                for (int i = sectionCreators.getLocation().getY(); i <= sectionDownload.getLocation().getY(); i += 10) {
                    jsExec.executeScript("window.scrollTo(0," + i + ");");

                    if (!sectionCreatorsFirstRow.getAttribute("class").contains("seen")) {
                        assertTrue(assertMessage,isVisible(sectionCreatorsHeader));
                        assertFalse(assertMessage,isVisibleArrayList(sectionCreators1SecondRow));
                        assertFalse(assertMessage,isVisible(sectionCreatorsExporeMore));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityFacebook));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityTwitter));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityInstagram));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                    } else if (sectionCreatorsFirstRow.getAttribute("class").contains("seen") && !sectionCreatorsSecondRow.getAttribute("class").contains("seen")) {
                        wait.until(ExpectedConditions.visibilityOf(sectionCreators1FirstRow.get(3)));
                        assertTrue(assertMessage,isVisible(sectionCreatorsHeader));
                        assertTrue(assertMessage,isVisibleArrayList(sectionCreators1FirstRow));
                        assertFalse(assertMessage,isVisible(sectionCreatorsExporeMore));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityFacebook));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityTwitter));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityInstagram));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                    } else if ((sectionCreatorsFirstRow.getAttribute("class").contains("seen") && sectionCreatorsSecondRow.getAttribute("class").contains("seen")) && !sectionCreatorsExporeMore.getAttribute("class").contains("seen")) {
                        wait.until(ExpectedConditions.visibilityOf(sectionCreators1SecondRow.get(3)));
                        assertTrue(assertMessage,isVisible(sectionCreatorsHeader));
                        assertTrue(assertMessage,isVisibleArrayList(sectionCreators1FirstRow));
                        assertTrue(assertMessage,isVisibleArrayList(sectionCreators1SecondRow));
                        assertFalse(assertMessage,isVisible(sectionCreatorsExporeMore));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityFacebook));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityTwitter));
                        assertFalse(assertMessage,isVisible(sectionCreatorsCommunityInstagram));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                    } else if (sectionCreatorsExporeMore.getAttribute("class").contains("seen") && !sectionCreatorsCommunitySocial.getAttribute("class").contains("seen")) {
                        wait.until(ExpectedConditions.visibilityOf(sectionCreatorsExporeMore));
                        assertTrue(assertMessage,isVisible(sectionCreatorsHeader));
                        assertTrue(assertMessage,isVisibleArrayList(sectionCreators1FirstRow));
                        assertTrue(assertMessage,isVisibleArrayList(sectionCreators1SecondRow));
                        assertTrue(assertMessage,isVisible(sectionCreatorsExporeMore));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                    } else {
                        wait.until(ExpectedConditions.visibilityOf(sectionCreatorsCommunityFacebook));
                        assertTrue(assertMessage,isVisible(sectionCreatorsHeader));
                        assertTrue(assertMessage,isVisibleArrayList(sectionCreators1FirstRow));
                        assertTrue(assertMessage,isVisibleArrayList(sectionCreators1SecondRow));
                        assertTrue(assertMessage,isVisible(sectionCreatorsExporeMore));
                        assertTrue(assertMessage,isVisible(sectionCreatorsCommunityFacebook));
                        assertTrue(assertMessage,isVisible(sectionCreatorsCommunityTwitter));
                        assertTrue(assertMessage,isVisible(sectionCreatorsCommunityInstagram));
                        assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                        assertTrue(driver.findElement(By.xpath("//a[contains(.,'Community')]")).getAttribute("class").contains("active"));
                        break;
                    }

                }

                //SCROLL RIGHT AND CHECK ALL THE PICTURES VISIBILITY, THEN SCROLL BACK
                jsExec.executeScript("arguments[0].scrollIntoView()", sectionCreators1FirstRow.get(0));
                if (driver.manage().window().getSize().getWidth() >= 1050) {
                    sectionCreatorsNavigationRight.click();
                    sleep(1000);
                    assertTrue(assertMessage,isVisibleArrayList(sectionCreators2));
                    sectionCreatorsNavigationRight.click();
                    sleep(1000);
                    assertTrue(assertMessage,isVisibleArrayList(sectionCreators3));
                    sectionCreatorsNavigationRight.click();
                    sleep(1000);
                    assertTrue(assertMessage,isVisibleArrayList(sectionCreators4));
                    sectionCreatorsNavigationRight.click();
                    sleep(1000);
                    assertTrue(assertMessage,isVisibleArrayList(sectionCreators5));
                    sectionCreatorsNavigationLeft.click();
                    sectionCreatorsNavigationLeft.click();
                    sectionCreatorsNavigationLeft.click();
                    sectionCreatorsNavigationLeft.click();
                }
            } else {
                //SMALL SCREEN ELEMENTS
                WebElement sectionCreatorsHeader = driver.findElement(By.xpath("//h2[contains(.,'A Community of Creators')]"));
                WebElement sectionCreatorsP = driver.findElement(By.xpath("//p[contains(.,'Join our group to see how others are using Cameo to make beautiful videos')]"));
                WebElement sectionCreatorsPic1 = driver.findElement(By.xpath("//img[contains(@src,'https://i.vimeocdn.com/video/602737860_640x360.jpg?r=pad')]"));
                WebElement sectionCreatorsPic2 = driver.findElement(By.xpath("//img[contains(@src,'https://i.vimeocdn.com/video/577587759_640x360.jpg?r=pad')]"));
                WebElement sectionCreatorsPic3 = driver.findElement(By.xpath("//img[contains(@src,'https://i.vimeocdn.com/video/577279824_640x360.jpg?r=pad')]"));
                WebElement sectionCreatorsPic4 = driver.findElement(By.xpath("//img[contains(@src,'https://i.vimeocdn.com/video/577275610_640x360.jpg?r=pad')]"));
                WebElement sectionCreatorsExporeMore = driver.findElement(By.xpath("//a[contains(.,'Explore More')]"));
                WebElement sectionCreatorsCommunityFacebook = driver.findElement(By.xpath("//div[contains(@class,'facebook')]"));
                WebElement sectionCreatorsCommunityTwitter = driver.findElement(By.xpath("//div[contains(@class,'twitter')]"));
                WebElement sectionCreatorsCommunityInstagram = driver.findElement(By.xpath("//div[contains(@class,'instagram')]"));
                WebElement sectionCreatorsPic5 = driver.findElement(By.xpath("//img[@src='https://i.vimeocdn.com/video/577077610_640x360.jpg?r=pad']"));

                assertTrue(assertMessage,isVisible(sectionCreatorsHeader));
                assertTrue(assertMessage,isVisible(sectionCreatorsP));
                assertTrue(assertMessage,isVisible(sectionCreatorsPic1));
                assertTrue(assertMessage,isVisible(sectionCreatorsPic2));
                assertTrue(assertMessage,isVisible(sectionCreatorsPic3));
                assertTrue(assertMessage,isVisible(sectionCreatorsPic4));
                assertTrue(assertMessage,isVisible(sectionCreatorsExporeMore));
                assertTrue(assertMessage,isVisible(sectionCreatorsCommunityFacebook));
                assertTrue(assertMessage,isVisible(sectionCreatorsCommunityTwitter));
                assertTrue(assertMessage,isVisible(sectionCreatorsCommunityInstagram));
                assertFalse(assertMessage,isVisible(sectionCreatorsPic5));
            }
        }
    }



    @Test
    public void testCaseDownload() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{767,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement sectionDownload = driver.findElement(By.id("download"));
            WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));
            jsExec.executeScript("arguments[0].scrollIntoView()", sectionDownload);
            sleep(1000);
            if (sectionIntroSectionMenu.isDisplayed()) {

                //DOWNLOAD ELEMENTS
                WebElement sectionDownloadHeader = driver.findElement(By.xpath("//h2[contains(.,'Go On, Make Your Movie')]"));
                WebElement sectionDownloadAppleLink = driver.findElement(By.xpath("//a[@class='badge_app_store']"));
                WebElement sectionDownloadTextMeALink = driver.findElement(By.xpath("//div[@class='input_overlay instruction']"));
                WebElement sectionDownloadScrollUp = driver.findElement(By.xpath("//div[@class='icon icon_arrow_up_black']"));

                wait.until(ExpectedConditions.visibilityOf(sectionDownloadHeader));

                assertTrue(assertMessage,isVisible(sectionDownloadHeader));
                assertTrue(assertMessage,isVisible(sectionDownloadAppleLink));
                assertTrue(assertMessage,isVisible(sectionDownloadTextMeALink));
                assertTrue(assertMessage,isVisible(sectionDownloadScrollUp));
                assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                assertTrue(driver.findElement(By.xpath("//a[contains(.,'Download')]")).getAttribute("class").contains("active"));
            } else {
                //SMALL SCREEN ELEMENTS
                WebElement sectionDownloadHeader = driver.findElement(By.xpath("//h2[contains(.,'Go On, Make Your Movie')]"));
                WebElement sectionDownloadAppleLink = driver.findElement(By.xpath("//a[@class='badge_app_store']"));
                WebElement sectionDownloadScrollUp = driver.findElement(By.xpath("//div[contains(@class,'icon icon_arrow_up_black')]"));
                WebElement sectionDownloadTextMeALink = driver.findElement(By.xpath("//div[@class='input_overlay instruction']"));

                assertTrue(assertMessage,isVisible(sectionDownloadHeader));
                assertTrue(assertMessage,isVisible(sectionDownloadAppleLink));
                assertTrue(assertMessage,isVisible(sectionDownloadScrollUp));
                assertFalse(assertMessage,isVisible(sectionDownloadTextMeALink));
            }
        }
    }


    @Test
    public void testCaseFooter() {
        int[][] browserSize = {{browserWidthInMax,browserHeightInMax},{1200,600},{767,500}};

        for (int b = 0 ; b < browserSize.length ; b++) {
            driver.manage().window().setSize(new Dimension(browserSize[b][0], browserSize[b][1]));
            assertMessage = "The browser size was width: " + browserSize[b][0] + ", height: " + browserSize[b][1];

            //MAIN ELEMENTS
            WebElement Footerlinks = driver.findElement(By.xpath("//section[@class='footer_v2__sitemap']"));
            WebElement FooterAuxiliary = driver.findElement(By.xpath("//section[@class='footer_v2__auxiliary ']"));
            WebElement sectionIntroSectionMenu = driver.findElement(By.xpath("//ul[@class='section_menu']"));

            //FOOTER ELEMENTS
            WebElement FooterlinksVimeo = driver.findElement(By.xpath("//div[@id='wrap']/footer/section/div/section/h4/span[contains(.,'Vimeo')]"));
            WebElement FooterlinksHelp = driver.findElement(By.xpath("//div[@id='wrap']/footer/section/div/section[2]/h4/span[contains(.,'Help')]"));
            WebElement FooterlinksMore = driver.findElement(By.xpath("//div[@id='wrap']/footer/section/div/section[3]/h4/span[contains(.,'More')]"));
            WebElement FooterlinksUpgrade = driver.findElement(By.xpath("//div[@id='wrap']/footer/section/div/section[4]/h4/span[contains(.,'Upgrade')]"));
            WebElement FooterlinksDidYouKnow = driver.findElement(By.xpath("//h4[contains(.,'Did you know?')]"));
            WebElement FooterAuxiliaryLegal = driver.findElement(By.xpath("//div[contains(@class,'footer_v2__legal')]"));
            WebElement FooterAuxiliaryFilter = driver.findElement(By.xpath("//div[contains(@class,'footer_v2__filters footer_v2__auxiliary-content')]"));

            jsExec.executeScript("arguments[0].scrollIntoView()", Footerlinks);

            assertTrue(assertMessage,isVisible(FooterlinksVimeo));
            assertTrue(assertMessage,isVisible(FooterlinksHelp));
            assertTrue(assertMessage,isVisible(FooterlinksMore));
            assertTrue(assertMessage,isVisible(FooterlinksUpgrade));
            assertTrue(assertMessage,isVisible(FooterlinksDidYouKnow));
            assertTrue(assertMessage,isVisible(FooterAuxiliaryLegal));
            assertTrue(assertMessage,isVisible(FooterAuxiliaryFilter));
            sleep(1000);
            if (sectionIntroSectionMenu.isDisplayed()) {
                assertTrue(assertMessage,isVisible(sectionIntroSectionMenu));
                assertTrue(driver.findElement(By.xpath("//a[contains(.,'Download')]")).getAttribute("class").contains("active"));
            }
        }
    }

    @After
    public void closeBrowser() {

        driver.close();

    }


    @AfterClass
    public static void tearDownOnce() {

        driver.quit();
    }
}