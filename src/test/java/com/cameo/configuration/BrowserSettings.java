package com.cameo.configuration;

/**
 * Set constants in related with browsers and its drivers
 * CHROME_DRIVER_LOCATION must set!!!
 */

public interface BrowserSettings {
    //CHROME SETTINGS
    String WEBDRIVER_CHROME_DRIVER = "chrome";
    String CHROME_DRIVER_LOCATION = "d:\\Repositories\\chromedriver.exe"; //this must set
    String CHROME_BROWSER_TYPE = "Chrome";


    //FIREFOX SETTINGS
    String WEBDRIVER_GECKO_DRIVER = "gecko";
    String GECKO_DRIVER_LOCATION = "d:\\Repositories\\geckodriver.exe"; //this must set
    String FIREFOX_BROWSER_TYPE = "Firefox";

}
