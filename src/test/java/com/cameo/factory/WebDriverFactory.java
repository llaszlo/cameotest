package com.cameo.factory;
/**
 * Input: string. ( E.g. CHROME_DRIVER )
 * return with a Webdriver based on input string
 */

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class WebDriverFactory {
    public enum WebDrivers {
        CHROME_DRIVER,
        FIREFOX_DRIVER
    }

    public static WebDriver getWebDriver(WebDrivers driverType) {
        WebDriver driver = null;
        if (WebDrivers.CHROME_DRIVER.equals(driverType)) {
            driver = new ChromeDriver();
        }
        if (WebDrivers.FIREFOX_DRIVER.equals(driverType)) {
            driver = new FirefoxDriver();
        }
        return driver;
    }
}
