package com.cameo.helper;

/**
 * These methods will create an account in the "https://vimeo.com/cameo" webpage with the given data provided by the .\configuration\UserSettings.java
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.cameo.configuration.UserSettings.EMAIL;
import static com.cameo.configuration.UserSettings.PASSWORD;
import static com.cameo.configuration.UserSettings.SIGNUP_NAME;
import static com.cameo.helper.ThreadSleepSupport.sleep;

public class Join {
    private static final String NO_THANKS = "No thanks »";

    private static WebDriver driver;

    private WebDriverWait wait;
    private String browserType;
    private FillInputField fillInputField;
    private LogIn logIn;
    private DeleteAccount deleteAccount;

    public Join(WebDriver driver, WebDriverWait wait, String browserType) {
        Join.driver = driver;
        this.wait = wait;
        this.browserType = browserType;
        this.fillInputField = new FillInputField(driver);
        this.logIn = new LogIn(driver, wait, browserType);
        this.deleteAccount = new DeleteAccount(driver, wait, browserType);
    }


    public void join() {

        Join.driver.navigate().to("https://vimeo.com/join");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signup_password")));

        fillInputField.fillInput(Join.driver.findElement(By.id("signup_name")),SIGNUP_NAME,browserType);

        fillInputField.fillInput(Join.driver.findElement(By.xpath("//input[contains(@class,'iris_form_text iris_form_text--lg  js-join_email')]")),EMAIL,browserType);

        fillInputField.fillInput(Join.driver.findElement(By.id("signup_password")),PASSWORD,browserType);

        Join.driver.findElement(By.xpath("//input[@value='Join with email']")).click();
    }

    public void joinIsPossible() {

        //CHECKING WHETHER THIS USER IS ALREADY REGISTERED, IF SO DELETE IT IN ORDER TO THE TEST COULD RUN PROPERLY
        sleep(2000);
        if (!(Join.driver.findElements(By.xpath("//a[contains(.,'Want to log in?')]")).size() == 0)) {
            logIn.logIn();
            deleteAccount.deleteAccount();
            join();
            joinIsPossible();
        } else {
            sleep(2000);
            if (!(Join.driver.findElements(By.linkText(NO_THANKS)).isEmpty())) {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(NO_THANKS)));
                Join.driver.findElement(By.linkText(NO_THANKS)).click();
            }
        }
    }




}