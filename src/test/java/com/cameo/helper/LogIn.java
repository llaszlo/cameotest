package com.cameo.helper;

/**
 * This method will login in the "https://vimeo.com/cameo" webpage with the given data provided by the .\configuration\UserSettings.java
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.cameo.configuration.UserSettings.EMAIL;
import static com.cameo.configuration.UserSettings.PASSWORD;

public class LogIn {
    private static WebDriver driver;

    private WebDriverWait wait;
    private String browserType;
    private FillInputField fillInputField;

    public LogIn(WebDriver driver, WebDriverWait wait, String browserType) {
        LogIn.driver = driver;
        this.wait = wait;
        this.browserType = browserType;
        this.fillInputField = new FillInputField(driver);
    }

    public void logIn() {

        LogIn.driver.navigate().to("https://vimeo.com/log_in");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login_password")));

        fillInputField.fillInput(LogIn.driver.findElement(By.xpath("//input[@class='iris_form_text iris_form_text--lg js-login_email']")),EMAIL,browserType);

        fillInputField.fillInput(LogIn.driver.findElement(By.id("login_password")),PASSWORD,browserType);

        LogIn.driver.findElement(By.xpath("//input[@value='Log in with email']")).click();
    }
}
