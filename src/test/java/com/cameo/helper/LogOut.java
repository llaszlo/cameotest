package com.cameo.helper;

/**
 * This method will logout in the "https://vimeo.com/cameo" webpage
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogOut {
    private static WebDriver driver;

    private WebDriverWait wait;
    private Actions action;

    public LogOut(WebDriver driver, WebDriverWait wait) {
        LogOut.driver = driver;
        this.wait = wait;
        this.action = new Actions(driver);
    }

    public void logOut() {

        driver.navigate().to("https://vimeo.com/home");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='topnav_menu_avatar']/a/img")));

        WebElement smileImg = driver.findElement(By.xpath("//div[@id='topnav_menu_avatar']/a/img"));
        WebElement logOutButton = driver.findElement(By.xpath("//button[contains(.,'Log out')]"));

        action.moveToElement(smileImg).perform();
        wait.until(ExpectedConditions.visibilityOf(logOutButton));

        action.click(logOutButton).perform();
    }
}
