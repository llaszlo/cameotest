package com.cameo.helper;

/**
 * These methods decide whether a webelement or list of webelements are visible when you scroll a webpage
 */

import org.openqa.selenium.WebElement;

import java.util.List;

public class IsWebElementVisible {


    public static boolean isVisible(WebElement x) {
        return x.isDisplayed();
    }

    public static boolean isVisibleArrayList(List<WebElement> x) {
        for (int i=0;i < x.size();i++) {
            if (!x.get(i).isDisplayed()) {
                return false;
            }
        }
        return true;
    }
}
