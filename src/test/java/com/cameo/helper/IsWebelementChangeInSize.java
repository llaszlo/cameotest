package com.cameo.helper;

/**
 * these methods help you to check if you scroll a webpage whether a webelement is changing in size ( shrink or enlarge )
 * xBefore: x coordinate of the webelement before changing ( you can get it e.g. Webelement.getLocation().getX())
 * yBefore: y coordinate of the webelement before changing ( you can get it e.g. Webelement.getLocation().getY())
 * xAfter: x coordinate of the webelement after changing ( you can get it e.g. Webelement.getLocation().getX())
 * yAfter: y coordinate of the webelement after changing ( you can get it e.g. Webelement.getLocation().getY())
 */

public class IsWebElementChangeInSize {


    public static boolean isShrink(int xBefore, int yBefore, int xAfter, int yAfter) {
        if (!(xBefore < xAfter && yBefore < yAfter)) {
            return false;
        }
        return true;
    }

    public static boolean isEnlarge(int xBefore, int yBefore, int xAfter, int yAfter) {
        if (!(xBefore > xAfter && yBefore > yAfter)) {
            return false;
        }
        return true;
    }
}
