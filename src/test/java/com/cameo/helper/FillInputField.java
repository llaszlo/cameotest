package com.cameo.helper;

/**
 * This method will write in input fields
 * element: The input field element of the page
 * inputText: the desired text to the input field
 * browserType: type of the browser ( e.g. Chrome or Firefox )
 */

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FillInputField {
    private static WebDriver driver;

    public FillInputField(WebDriver driver) {
        FillInputField.driver = driver;
    }

    public void fillInput(WebElement element, String inputText, String browserType) {
        element.click();
        element.clear();
        if ("Chrome".equals(browserType)) {
            element.sendKeys(inputText);
        }
        if ("Firefox".equals(browserType)) {
            String x = "arguments[0].setAttribute('value', '"+inputText+"')";
            ((JavascriptExecutor) driver).executeScript(x, element);
        }
    }

}
