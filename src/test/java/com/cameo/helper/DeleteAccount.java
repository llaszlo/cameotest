package com.cameo.helper;

/**
 * This method will delete the account in the "https://vimeo.com/cameo" webpage with the given data provided by the .\configuration\UserSettings.java
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.cameo.configuration.UserSettings.PASSWORD;
import static com.cameo.helper.ThreadSleepSupport.sleep;

public class DeleteAccount {
    private static WebDriver driver;

    private WebDriverWait wait;
    private Actions action;
    private String browserType;
    private FillInputField fillInputField;

    public DeleteAccount(WebDriver driver, WebDriverWait wait, String browserType) {
        DeleteAccount.driver = driver;
        this.wait = wait;
        this.action = new Actions(driver);
        this.browserType = browserType;
        this.fillInputField = new FillInputField(driver);
    }

    public void deleteAccount() {

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='topnav_menu_avatar']/a/img")));
        sleep(2000);
        WebElement smileImg = DeleteAccount.driver.findElement(By.xpath("//div[@id='topnav_menu_avatar']/a/img"));
        action.moveToElement(smileImg).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(@title,'Bye for now, friend')]")));

        WebElement accountSettingsButton = DeleteAccount.driver.findElement(By.xpath("//a[@href='//vimeo.com/settings']"));
        action.click(accountSettingsButton).perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(.,'Delete your account')]")));

        DeleteAccount.driver.findElement(By.xpath("//a[contains(.,'Delete your account')]")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='Delete account']")));

        fillInputField.fillInput(DeleteAccount.driver.findElement(By.id("password")),PASSWORD,browserType);

        DeleteAccount.driver.findElement(By.xpath("//input[@value='Delete account']")).click();
    }
}
