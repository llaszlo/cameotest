Before start you must set the directory path of geckodriver.exe and chromedriver.exe in the code. You can find the code here in this file: \configuration\BrowserSettings.java. e.g.: String CHROME_DRIVER_LOCATION = "d:\\Repositories\\chromedriver.exe"; //this must set

to run this code type in the following maven command:
mvn clean test


General bugs in geckodriver:
	- sendKeys does not work
	- moveToElement does not work if the mouse is on the browser
	- setSize(), getSize() methods don't work. 


.\configuration\BrowserSettings.java interface:
	it contains constants in related with browser types

.\configuration\UserSettings.java interface:
	it contains constants in related with user
	
.\factory\WebDriverFactory.java class:
	it gets a string and produce a Webdriver back based on the input string

.\test\base\JoinAndLogin.java class:
	abstract parent class of the TestJoinAndLoginInChrome.java and TestJoinAndLoginInFirefox.java classes

.\test\joinandlogin\TestJoinAndLoginInFirefox.java and .\test\joinandlogin\TestJoinAndLoginInChrome.java classes:
	- goal of these tests is the same but in different browsers to test whether a user can register on the "https://vimeo.com/cameo" webpage and after it the user can login with this registration. This is conducted by the "TestCaseJoinAndLogInAndDeleteAccount" method.
	- these tests also check if a user has already registered he/she can't register with the same email address. This is conducted by the "TestCaseDoubleJoinIsAllowance" method.
	- Moreover these tests check if a user is not registered he/she can login anyway. This is conducted by the "TestCaseUnregisteredUserCanLogin" method.
	- running order of the tests are indifferent from each other


.\test\base\ScrollMainPage.java  class:
	- abstract parernt class of the TestScrollMainPageInChrome.java class
	- it contains 8 @test annotations. Each of them test 1 section on the main page.
	
.\test\scroll\TestScrollMainPageInChrome.java class:
	- its goal is to test the changing of the main page elements by scrolling on the "https://vimeo.com/cameo" webpage.
	- running order of the tests are indifferent from each other
	- in all sections ( except testCaseFooter section ) the outermost if-else condition's goal is to dinamically determine whether the browser is in small screen or in big screen. The "if" blocks contain the big screen tests. The "else" blocks contain the small screen tests.

.\helper package:
	These classes contain methods which you can use anywhere
	
	